# Weeport contribution guide and tips

## Generate new GTK types methods to get from ui builder

We are using a builder element (in `internal/builder`) to extract typed elements from the ui file in methods like
`builder.GetButton(name)`.
As generating those is cumbersome, there is a generator to creates `internal/builder/types.go` with those methods.
Adding a new element is simply:
 1. Edit `internal/builder/gentypes.go`, add a new string matching the gtk element type in the `types` slice.
 1. Run `go generate` anywhere in the project
 1. Now, `types.go` will contain the new `builder.GetFoo(name)` element. Do not forget to commit this file as well.

## Iterating with local assets

By default, `go build` will embedded the assets in the static binary. No need for the UI file being in a certain relative
to the binary location.

You should use `go generate` to regenerates the assets to be embedded in the binary itself.

However, it's better sometimes to iterate over the assets, like the `.ui` file without recompiling the whole binary. For this,
instead of `go build`, run `go build --tags=dev`. This will use the on-disk assets. Note that their absolute path is recorded
inside the binary (to build able to run the binary from any location). If you thus change your project directory or the
asset location, just rebuild again the binary with the `dev` tag.

## Installing assets on system

You can use a special build which won't embedded assets or compiled translations in the binary itself, but will rely
on system data directories to find them at the usual places.

For this, just add `--tags=external` to generate and build:
```
go generate --tags=external ./...
go build --tags=external ./...
```

With the external tag:
 * `go generate` will try to install them on your system. You can change the destination via the traditional "$DESTDIR" and
"$PREFIX" environment variables, for package installation in particular.
 * `go build` will build a special version of the binary without any embedded assets/translations. It will know how to load
 them from the traditional directories on the system, like `XDG_DATA_DIRS` for assets and stop at the first directory it finds.

## Regenerate po/mo files or add new languages

Po files are generated and updated in the `po/` directory when running `go generate ./...`. Definition for package name,
files to include (file globbing expressions) and languages are in `po.i18n.yaml`. If you want to add a new language,
just append to locale keycode to the `language` list. `go generate ./...` will create the corresponding po file for you.

The same commands generates the `*.mo` assets in a temporary directory (those aren't supposed to be checked in). As with
normal assets, **mo** binary files can be either embedded in the binary (default), using directly `po/<locale>.po` files
with `go build --tags=dev` for testing local modification or installed in the system with `--tags=external` on both
go generate and go build (see previous section).

Note that `go generate` needs `gettext` utilities to be installed. `dev` mode weeport binary needs to execute on a system
which has `msgfmt` (from `gettext`) installed.
