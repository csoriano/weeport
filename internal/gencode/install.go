package gencode

import (
	"io"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"

	"golang.org/x/xerrors"
)

// DefaultPrefix used as prefix
const DefaultPrefix = "/usr/local"

// Install copies src to dest, prefixing as needed destdir/prefix/ and making sanity checks
func Install(src, dest, destdir, prefix string) error {

	// sanity check prefix
	if prefix == "" {
		prefix = DefaultPrefix
	}
	if !filepath.IsAbs(prefix) {
		return xerrors.New("ERROR: prefix should be an asbolute path")
	}

	// do installation
	dest = filepath.Join(destdir, prefix, dest)
	if err := CopyTree(src, dest); err != nil {
		if s, e := filepath.Abs(src); e == nil {
			src = s
		}
		return xerrors.Errorf("couldn't install %q to %q: %v", src, dest, err)
	}

	return nil
}

// copyFile copies a single file from src to dest
func copyFile(src, dest string) error {
	srcF, err := os.Open(src)
	if err != nil {
		return xerrors.Errorf("couldn't find %q: %v", src, err)
	}
	defer srcF.Close()

	destF, err := os.Create(dest)
	if err != nil {
		return xerrors.Errorf("couldn't create %q: %v", dest, err)
	}
	defer destF.Close()

	if _, err = io.Copy(destF, srcF); err != nil {
		return xerrors.Errorf("couldn't copy content: %v", err)
	}

	srcInfo, err := os.Stat(src)
	if err != nil {
		return xerrors.Errorf("couldn't stat %q: %v", src, err)
	}
	if err = os.Chmod(dest, srcInfo.Mode()); err != nil {
		return xerrors.Errorf("couldn't change mod of %q: %v", dest, err)
	}
	return nil
}

// CopyTree copies a whole directory recursively
func CopyTree(src string, dest string) error {
	srcInfo, err := os.Stat(src)
	if err != nil {
		return xerrors.Errorf("couldn't stat %q: %v", src, err)
	}

	if !srcInfo.IsDir() {
		return copyFile(src, dest)
	}

	if err = os.MkdirAll(dest, srcInfo.Mode()); err != nil {
		return xerrors.Errorf("couldn't create directory %q: %v", dest, err)
	}

	fds, err := ioutil.ReadDir(src)
	if err != nil {
		return xerrors.Errorf("couldn't stat %q: %v", src, err)
	}
	for _, fd := range fds {
		s := path.Join(src, fd.Name())
		d := path.Join(dest, fd.Name())

		if fd.IsDir() {
			if err = CopyTree(s, d); err != nil {
				return xerrors.Errorf("couldn't copy directories %q to %q: %v", s, d, err)
			}
		} else {
			if err = copyFile(s, d); err != nil {
				return xerrors.Errorf("couldn't copy file %q to %q: %v", s, d, err)
			}
		}
	}
	return nil
}
