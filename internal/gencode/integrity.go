package gencode

import (
	"bufio"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strings"

	"golang.org/x/xerrors"
)

// InstallCheckFlags returns check specific flags
func InstallCheckFlags() (check *bool, verbose *bool) {
	return flag.Bool("check", false, "enable check mode. Can also be set via GOGENERATE_CHECK=true. Returns != 0 if generated files mismatch sources"),
		flag.Bool("verbose", false, "print complete files content if differs and not only the error")
}

// IsCheckMode enhanced checkmod with an env variable GOGENERATE_CHECK
func IsCheckMode(checkMode bool) bool {
	if !checkMode && strings.ToLower(os.Getenv("GOGENERATE_CHECK")) == "true" {
		return true
	}
	return checkMode
}

// CreateTemp creates a temporary file and directory while returning teardown function
func CreateTemp(name string, directory bool) (string, func(), error) {
	var dest string
	var err error
	if directory {
		if dest, err = ioutil.TempDir("", "go-generate-"+filepath.Base(name)); err != nil {
			return "", func() {}, xerrors.Errorf("couldn't create temporary directory name:", err)
		}
	} else {
		f, err := ioutil.TempFile("", "go-generate-"+filepath.Base(name))
		if err != nil {
			return "", func() {}, xerrors.Errorf("couldn't create temporary file:", err)
		}
		dest = f.Name()
		f.Close()
	}

	return dest, func() {
		if directory {
			if err := os.RemoveAll(dest); err != nil {
				log.Println("couldn't remove temporary directory:", err)
			}
		} else {
			if err := os.Remove(dest); err != nil {
				log.Println("couldn't remove temporary file:", err)
			}
		}
	}, nil
}

func (g GenCode) same(generatedPath string) (bool, error) {
	var errs []error

	filesToCheck := make(map[string]string)
	if g.destIsDir {
		// prefix to strip from the path mapping ("root")
		prefix := g.Dest

		err := filepath.Walk(g.Dest, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return xerrors.Errorf("fail to access %q: %v", path, err)
			}

			relativePath := strings.TrimPrefix(strings.TrimPrefix(path, prefix), "/")
			destPath := filepath.Join(generatedPath, relativePath)
			for _, f := range g.FilesToIgnore {
				if f == relativePath {
					return nil
				}
			}

			if info.IsDir() {
				oFiles, err := ioutil.ReadDir(path)
				if err != nil {
					return xerrors.Errorf("couldn't list files in %q: %v", path, err)
				}
				dFiles, err := ioutil.ReadDir(destPath)
				if err != nil {
					return xerrors.Errorf("couldn't list files in %q: %v", destPath, err)
				}

				// filter files for potential ones in ignore list from origin
				var filteredOrigList []string
			buildfilter:
				for _, f := range oFiles {
					for _, i := range g.FilesToIgnore {
						if filepath.Join(relativePath, f.Name()) == i {
							continue buildfilter
						}
					}
					filteredOrigList = append(filteredOrigList, filepath.Join(relativePath, f.Name()))
				}

				if len(filteredOrigList) != len(dFiles) {
					var generatedList []string
					for _, f := range dFiles {
						generatedList = append(generatedList, f.Name())
					}
					return xerrors.Errorf("%d of filtered files in %q (%s), while %d was generated (%s)",
						len(filteredOrigList), path, filteredOrigList, len(dFiles), generatedList)
				}

				return nil
			}

			// strip root prefix as temporary root is <generatedPath>/<relativePath>
			filesToCheck[path] = destPath
			return nil
		})
		if err != nil {
			errs = append(errs, err)
		}
	} else {
		filesToCheck[g.Dest] = generatedPath
	}

	for origin, generated := range filesToCheck {

		identical, err := AreFilesIdentical(origin, generated, g.IgnoreInCheckFilters)
		if err != nil {
			if _, ok := err.(*os.PathError); ok {
				errs = append(errs, xerrors.Errorf("couldn't check equality: %v", err))
				continue
			}
			errs = append(errs, xerrors.Errorf("unexpected error: %v", err))
			continue
		}

		if !identical {
			if g.Verbose {
				b1, err := ioutil.ReadFile(origin)
				if err != nil {
					errs = append(errs, xerrors.Errorf("couldn't read original file: %v", err))
					continue
				}
				b2, err := ioutil.ReadFile(generated)
				if err != nil {
					errs = append(errs, xerrors.Errorf("couldn't read generated file: %v", err))
					continue
				}
				errs = append(errs, xerrors.Errorf("original and generated are differents:\n---- %s:\n%s\n---- generated:\n%s", generated, b1, b2))
				continue
			} else {
				errs = append(errs, xerrors.Errorf("original and generated are differents for %q", origin))
			}
		}
	}

	if errs != nil {
		var err string
		for _, e := range errs {
			err += fmt.Sprintf("* %v\n", e)
		}
		return false, xerrors.Errorf("some files differs:\n%s", err)
	}

	return true, nil
}

type scannerSkipper struct {
	*bufio.Scanner
	filters []*regexp.Regexp
}

// Scan advance to the next token which doesn't match any filters
func (s *scannerSkipper) Scan() bool {
scan:
	for {
		if done := s.Scanner.Scan(); !done {
			return done
		}

		t := s.Text()
		// Ignore t matching any filters
		for _, f := range s.filters {
			if f.MatchString(t) {
				continue scan
			}
		}

		return true
	}
}

// AreFilesIdentical compares orginal and generated files.
// filters regexps are used to ignore dates and other potential differences.
// TODO: will probably be better with some io.Reader
func AreFilesIdentical(originalPath, generatedPath string, filters []*regexp.Regexp) (bool, error) {
	origin, err := os.Open(originalPath)
	if err != nil {
		return false, xerrors.Errorf("couldn't open %s: %v", origin, err)
	}
	defer origin.Close()
	originScanner := scannerSkipper{bufio.NewScanner(origin), filters}

	generated, err := os.Open(generatedPath)
	if err != nil {
		return false, xerrors.Errorf("couldn't open generated file: %v", err)
	}
	defer generated.Close()
	generatedScanner := scannerSkipper{bufio.NewScanner(generated), filters}

	for {
		originDone := !originScanner.Scan()
		generatedDone := !generatedScanner.Scan()

		if originDone && generatedDone {
			break
		}

		// We are able to scan origin more than generated -> files are different
		if originDone != generatedDone {
			log.Printf("generated and origin file sizes for %q are different\n", originalPath)
			return false, nil
		}

		l := originScanner.Text()
		m := generatedScanner.Text()

		if l != m {
			log.Printf("difference detected for %q:\n\nIn tree:\n%s\n\nExpected:\n%s\n", originalPath, l, m)
			return false, nil
		}
	}

	if err := originScanner.Err(); err != nil {
		return false, err
	}
	if err := generatedScanner.Err(); err != nil {
		return false, err
	}

	return true, nil
}
