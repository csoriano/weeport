// +build ignore

// This programs install assets to destination directory. It can be invoked by running go generate
package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"gitlab.gnome.org/csoriano/weeport/internal/gencode"
)

func main() {
	destdir := flag.String("destdir", "", "Prepend destdir to destination path")
	prefix := flag.String("prefix", gencode.DefaultPrefix, "Prepend prefix (absolute path) between destdir and destination path")
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage of %s:\n    %s SOURCE DESTINATION\n", os.Args[0], os.Args[0])
		fmt.Fprintln(os.Stderr, "Install SOURCE content to <DESTDIR>/<PREFIX>/DESTINATION")
	}
	flag.Parse()
	args := flag.Args()
	if len(args) != 2 {
		flag.Usage()
		os.Exit(1)
	}

	if err := gencode.Install(args[0], args[1], *destdir, *prefix); err != nil {
		log.Fatalln(err)
	}
}
