// +build ignore

// This programs generates data.go. It can be invoked by running go generate
package main

import (
	"flag"
	"log"
	"net/http"
	"regexp"

	"gitlab.gnome.org/csoriano/weeport/internal/gencode"
	"golang.org/x/xerrors"

	"github.com/shurcooL/vfsgen"
)

const target = "data.go"

type assetsGenerator struct{}

func (assetsGenerator) Generate(dest string) error {
	if err := vfsgen.Generate(http.Dir("data"), vfsgen.Options{
		Filename:        dest,
		PackageName:     "assets",
		BuildTags:       "!dev,!external",
		VariableName:    "Data",
		VariableComment: "Data are the embedded assets withing the binary, corresponding to internal/assets/data",
	}); err != nil {
		return xerrors.Errorf("couldn't generate new assets file: %v", err)
	}

	return nil
}

func main() {
	checkMode, verbose := gencode.InstallCheckFlags()
	flag.Parse()
	*checkMode = gencode.IsCheckMode(*checkMode)

	g := gencode.GenCode{
		Generator: assetsGenerator{},
		Dest:      target,

		CheckMode: *checkMode,
		Verbose:   *verbose,
		IgnoreInCheckFilters: []*regexp.Regexp{
			regexp.MustCompile(` *modTime: *time.Date`),
		},
	}

	err := g.GenerateAndCheck()
	if err != nil {
		log.Fatalf("couldn't generate assets: %+v", err)
	}
	defer g.Close()
}
