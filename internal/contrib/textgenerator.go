package contrib

// TextGenerator is the text formater of our report
type TextGenerator struct {
}

func (TextGenerator) header() string {
	return ""
}

func (TextGenerator) projectTemplate() string {
	return textProjectTemplate
}

func (TextGenerator) actionTemplate() string {
	return textActionTemplate
}

func (TextGenerator) contributionTemplate() string {
	return textContributionTemplate
}

func (TextGenerator) sectionEnd() string {
	return ""
}

func (TextGenerator) end() string {
	return ""
}

const textProjectTemplate = `
{{.}}
⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺⎺
`
const textActionTemplate = `  ◽ {{.}}
`
const textContributionTemplate = `{{if .ReportTitle}}    ◾ {{.ReportTitle}}
{{else}}    ◾ {{.Title}}
{{end}}       {{.Link}}
{{if .ReportDescription}}          ⟶ {{.ReportDescription}}
{{end}}`
