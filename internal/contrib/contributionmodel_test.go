package contrib_test

import (
	"testing"

	. "gitlab.gnome.org/csoriano/weeport/internal/contrib"
)

func TestModelContributions(t *testing.T) {
	t.Parallel()
	testCases := []struct {
		name string

		cm Model

		want int
	}{
		{"Single subsection",
			Model{
				Sections: []*Section{
					&Section{
						Subsections: []*Section{
							&Section{
								Contributions: []*Contribution{
									&Contribution{ProjectName: "project1", TargetType: "target1", ActionName: "action1"},
								},
							},
						},
					},
				},
			},
			1},
		{"multiple subsections",
			Model{
				Sections: []*Section{
					&Section{
						Subsections: []*Section{
							&Section{
								Contributions: []*Contribution{
									&Contribution{ProjectName: "project1", TargetType: "target1", ActionName: "action1"},
								},
							},
							&Section{
								Contributions: []*Contribution{
									&Contribution{ProjectName: "project1", TargetType: "target1", ActionName: "action1"},
								},
							},
						},
					},
				},
			},
			2},
		{"multiple sections",
			Model{
				Sections: []*Section{
					&Section{
						Subsections: []*Section{
							&Section{
								Contributions: []*Contribution{
									&Contribution{ProjectName: "project1", TargetType: "target1", ActionName: "action1"},
								},
							},
							&Section{
								Contributions: []*Contribution{
									&Contribution{ProjectName: "project1", TargetType: "target1", ActionName: "action1"},
								},
							},
						},
					},
					&Section{
						Subsections: []*Section{
							&Section{
								Contributions: []*Contribution{
									&Contribution{ProjectName: "project1", TargetType: "target1", ActionName: "action1"},
								},
							},
							&Section{
								Contributions: []*Contribution{
									&Contribution{ProjectName: "project1", TargetType: "target1", ActionName: "action1"},
								},
							},
						},
					},
				},
			},
			4},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			if got := len(tc.cm.Contributions()); got != tc.want {
				t.Errorf("Contributions() doesn't match: got %v, want %v", got, tc.want)
			}
		})
	}
}

func TestModelRemove(t *testing.T) {
	t.Parallel()
	testCases := []struct {
		name string

		cm      Model
		contrib Contribution

		want int
	}{
		{"empty ID",
			Model{
				Sections: []*Section{
					&Section{
						Subsections: []*Section{
							&Section{
								Contributions: []*Contribution{
									&Contribution{ProjectName: "project1", TargetType: "target1", ActionName: "action1"},
								},
							},
						},
					},
				}},
			Contribution{ProjectName: "project1", TargetType: "target1", ActionName: "action1"},
			0},
		{"Some matching ID",
			Model{
				Sections: []*Section{
					&Section{
						Subsections: []*Section{
							&Section{
								Contributions: []*Contribution{
									&Contribution{ID: 1, ProjectName: "project1", TargetType: "target1", ActionName: "action1"},
								},
							},
						},
					},
				}},
			Contribution{ID: 1, ProjectName: "project1", TargetType: "target1", ActionName: "action1"},
			0},
		{"No matching ID",
			Model{
				Sections: []*Section{
					&Section{
						Subsections: []*Section{
							&Section{
								Contributions: []*Contribution{
									&Contribution{ID: 1, ProjectName: "project1", TargetType: "target1", ActionName: "action1"},
								},
							},
						},
					},
				}},
			Contribution{ID: 2, ProjectName: "project1", TargetType: "target1", ActionName: "action1"},
			1},
	}

	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()
			tc.cm.Remove(&tc.contrib)

			if got := len(tc.cm.Contributions()); got != tc.want {
				t.Errorf("Remove() doesn't match: got %v, want %v", got, tc.want)
			}
		})
	}
}
