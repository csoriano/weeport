package contrib

// HTMLGenerator is the HTML formater of our report
type HTMLGenerator struct {
}

func (HTMLGenerator) header() string {
	return htmlHeader
}

func (HTMLGenerator) projectTemplate() string {
	return htmlProjectTemplate
}

func (HTMLGenerator) actionTemplate() string {
	return htmlActionTemplate
}

func (HTMLGenerator) contributionTemplate() string {
	return htmlContributionTemplate
}

func (HTMLGenerator) sectionEnd() string {
	return htmlDivEnd
}

func (HTMLGenerator) end() string {
	return htmlEnd
}

const htmlHeader = `
<html>
<head>
<link rel="stylesheet" type="text/css" href="style.css" media="screen" />
<title>Weeport Weekly Report</title>
</head>
<body>
`
const htmlProjectTemplate = `
<div class="project">
<p class="project-title">{{.}}</p>
`
const htmlActionTemplate = `
<div class="action">
<p class="action-title">{{.}}</p>
<div class="items">
`
const htmlContributionTemplate = `
<p>
{{if .Link}}
• <a href={{.Link}}>Link</a> – {{if .ReportTitle}}{{.ReportTitle}}{{else}}{{.Title}}{{end}}
{{else}}
• {{if .ReportTitle}}{{.ReportTitle}}{{else}}{{.Title}}{{end}}
{{end}}
</p>
{{if .ReportDescription}}
<p class="description">{{.ReportDescription}}</p>
{{end}}`

const htmlDivEnd = `
</div>
`

const htmlEnd = `
</body>
</html>
`
