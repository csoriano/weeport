package app

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/url"
	"os"

	"gitlab.gnome.org/csoriano/weeport/internal/assets"
	"gitlab.gnome.org/csoriano/weeport/internal/builder"
	"gitlab.gnome.org/csoriano/weeport/internal/contrib"
	"gitlab.gnome.org/csoriano/weeport/internal/gitlabfetcher"
	"golang.org/x/xerrors"

	"github.com/gotk3/gotk3/gdk"
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
)

const (
	MODE_EDIT = iota
	MODE_RESULT
)

type sectionUI struct {
	section *contrib.Section

	titleWidget            *gtk.Label
	contributionsContainer *gtk.ListBox
	mainContainer          *gtk.Box
	contributionsUI        []*contributionUI

	subsectionsUI []*sectionUI
}

type contributionUI struct {
	contribution *contrib.Contribution
	widget       *gtk.ListBoxRow
}

type mainWindow struct {
	GtkObject             *gtk.ApplicationWindow
	headerBar             *gtk.HeaderBar
	mainStack             *gtk.Stack
	downloadProgressLabel *gtk.Label
	content               *gtk.Box

	model           *contrib.Model
	contributionsUI []sectionUI

	errorFormat string
	debug       bool
}

func newMainWindow(debug bool, errorFormat string) (*mainWindow, error) {
	w := mainWindow{
		debug:       debug,
		errorFormat: errorFormat,
	}

	f, err := assets.Data.Open("ui/main.ui")
	if err != nil {
		// Wrap here as vfs generated code returns error from stdlib and not xerrors
		return nil, xerrors.Errorf("couldn't open: "+errorFormat, err)
	}
	defer f.Close()

	w.setUpCSS()

	builder, err := builder.New(f)
	if err != nil {
		return nil, err
	}

	if w.GtkObject, err = builder.GetApplicationWindow("mainWindow"); err != nil {
		return nil, err
	}

	if w.content, err = builder.GetBox("contributionsContainer"); err != nil {
		return nil, err
	}

	if w.headerBar, err = builder.GetHeaderBar("headerBar"); err != nil {
		return nil, err
	}

	generateButton, err := builder.GetButton("generateButton")
	if err != nil {
		return nil, err
	}
	generateButton.Connect("clicked", func() {
		// Print as it's a callback error in correct format
		var returnErr error
		defer func() {
			if returnErr != nil {
				log.Printf("ERROR: couldn't generate report: "+errorFormat, returnErr)
			}
		}()

		src, err := assets.Data.Open("web/style.css")
		if err != nil {
			returnErr = xerrors.Errorf("couldn't open source file 'style.css': %v", err)
			return
		}
		defer src.Close()
		dst, err := os.Create("style.css")
		if err != nil {
			returnErr = xerrors.Errorf("couldn't open destination file 'style.css': %v", err)
			return
		}
		defer dst.Close()
		if _, err := io.Copy(dst, src); err != nil {
			returnErr = xerrors.Errorf("couldn't copy style.css: %v", err)
			return
		}
		f, err := os.Create("weeklyReport.html")
		if err != nil {
			returnErr = xerrors.Errorf("couldn't create html version weeklyReport: %v", err)
			return
		}
		defer f.Close()
		if err = w.model.Generate(contrib.HTMLGenerator{}, f); err != nil {
			returnErr = err
			return
		}
		f, err = os.Create("weeklyReport.txt")
		if err != nil {
			err = xerrors.Errorf("couldn't create text version weeklyReport: %v", err)
			return
		}
		defer f.Close()
		if err = w.model.Generate(contrib.TextGenerator{}, f); err != nil {
			returnErr = err
			return
		}
	})

	if w.downloadProgressLabel, err = builder.GetLabel("downloadProgressLabel"); err != nil {
		return nil, err
	}

	if err := w.setUpSaveToken(builder); err != nil {
		return nil, err
	}

	if w.mainStack, err = builder.GetStack("mainStack"); err != nil {
		return nil, err
	}

	if gitlabfetcher.CheckToken() {
		w.retrieveContributions()
	} else {
		w.mainStack.SetVisibleChildName("tokenPage")
	}
	w.GtkObject.SetDefaultSize(800, 600)
	w.GtkObject.ShowAll()

	a := glib.SimpleActionNew("edit-token", nil)
	a.Connect("activate", func() { w.mainStack.SetVisibleChildName("tokenPage") })
	w.GtkObject.AddAction(a)

	a = glib.SimpleActionNew("save-to-disk", nil)
	a.Connect("activate", func() {
		if err := w.model.SaveToDisk(); err != nil {
			log.Printf("ERROR: couldn't save cache to disk: "+w.errorFormat, err)
		}
	})
	w.GtkObject.AddAction(a)

	a = glib.SimpleActionNew("load-from-disk", nil)
	a.Connect("activate", func() {
		if w.model, err = contrib.NewFromDisk(); err != nil {
			if w.debug {
				log.Printf("DEBUG: couldn't find cache data from disk: "+errorFormat, err)
			}
			return
		}
		err := w.refreshContributions()
		if err != nil {
			log.Printf("ERROR: couldn't refresh contributions: "+w.errorFormat, err)
		}
		w.mainStack.SetVisibleChildName("itemsPage")
	})
	w.GtkObject.AddAction(a)

	return &w, nil
}

func (w *mainWindow) setUpCSS() error {
	f1, err := assets.Data.Open("ui/style.css")
	if err != nil {
		// Wrap here as vfs generated code returns error from stdlib and not xerrors
		return xerrors.Errorf("couldn't open: "+w.errorFormat, err)
	}
	defer f1.Close()
	css, err := gtk.CssProviderNew()
	if err != nil {
		return err
	}
	b, err := ioutil.ReadAll(f1)
	if err != nil {
		return xerrors.Errorf("couldn't read from reader: %v", err)
	}
	if err = css.LoadFromData(string(b)); err != nil {
		return xerrors.Errorf("invalid CSS content: %v", err)
	}

	screen, err := gdk.ScreenGetDefault()
	if err != nil {
		return err
	}
	gtk.AddProviderForScreen(screen, css, gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

	return nil
}

func (w *mainWindow) setUpSaveToken(builder *builder.Builder) error {
	tokenEntry, err := builder.GetEntry("tokenEntry")
	if err != nil {
		return err
	}

	tokenSaveButton, err := builder.GetButton("tokenSaveButton")
	if err != nil {
		return err
	}

	tokenSaveButton.Connect("clicked", func() {
		text, err := tokenEntry.GetText()
		if err != nil {
			log.Printf("ERROR: no token specified: "+w.errorFormat, err)
			return
		}
		if err := gitlabfetcher.SaveToken(text); err != nil {
			// Would be better to show some dialogs or UI for such things
			log.Printf("ERROR: couldn't save token to disk: "+w.errorFormat, err)
			return
		}
		w.retrieveContributions()
	})
	return nil
}

func (w *mainWindow) refreshContributions() error {
	w.clearContributions()
	return w.populateUI()
}

func (w *mainWindow) clearContributions() {
	for l := w.content.GetChildren(); l != nil; l = l.Next() {
		l.Data().(*gtk.Widget).Destroy()
	}
}

func (w *mainWindow) retrieveContributions() {
	w.mainStack.SetVisibleChildName("loadingPage")
	// As we have a main loop, we can't block UI events here via sync code here. So still send that through a goroutine
	go func() {
		progress := make(chan gitlabfetcher.DownloadProgressData)
		var contribs []contrib.Contribution
		var err error

		go func() {
			contribs, err = gitlabfetcher.GNOMEContributions(progress)
			close(progress)
		}()

		for progress != nil {
			select {
			case p, ok := <-progress:
				if !ok {
					progress = nil
					break
				}
				glib.IdleAdd(func() {
					w.downloadProgressLabel.SetMarkup(
						fmt.Sprintf("<span foreground=\"#42464c\" size=\"small\">%d/%d</span>",
							p.Current, p.Max))
				})
			}
		}

		// If any error after fetching: show token page
		if err != nil {
			log.Printf("WARNING: error when fetching contributions: "+w.errorFormat, err)
			glib.IdleAdd(func() {
				w.mainStack.SetVisibleChildName("tokenPage")
			})
			return
		}

		// Otherwise, fill the model and change page
		w.model = contrib.NewModel(contribs)
		glib.IdleAdd(func() {
			w.mainStack.SetVisibleChildName("itemsPage")
			if err := w.populateUI(); err != nil {
				log.Printf("ERROR: couldn't build contribution UI: "+w.errorFormat, err)
			}
		})
	}()
}

func (w *mainWindow) populateUI() error {
	for _, s := range w.model.Sections {
		if len(s.Contributions) == 0 && len(s.Subsections) == 0 {
			continue
		}
		sUI := sectionUI{section: s}
		project := s.Title
		sUI.titleWidget, _ = gtk.LabelNew(project)
		css, err := sUI.titleWidget.GetStyleContext()
		sUI.titleWidget.Show()
		if err != nil {
			return xerrors.Errorf("couldn't access css style")
		}
		css.AddClass("section-title")
		w.content.Add(sUI.titleWidget)
		w.contributionsUI = append(w.contributionsUI, sUI)
		for _, ss := range s.Subsections {
			if len(ss.Contributions) == 0 && len(ss.Subsections) == 0 {
				continue
			}
			ssUI := sectionUI{section: ss}
			ssUI.titleWidget, _ = gtk.LabelNew(ss.Title)
			ssUI.titleWidget.SetXAlign(0)
			css, err = ssUI.titleWidget.GetStyleContext()
			if err != nil {
				return xerrors.Errorf("couldn't access css style")
			}
			css.AddClass("subsection-title")
			ssUI.titleWidget.Show()
			w.content.Add(ssUI.titleWidget)
			ssUI.contributionsContainer, err = gtk.ListBoxNew()
			if err != nil {
				return xerrors.Errorf("couldn't build a subsection: %v", err)
			}
			ssUI.contributionsContainer.Show()
			css, err = ssUI.contributionsContainer.GetStyleContext()
			if err != nil {
				return xerrors.Errorf("couldn't get css style: %v", err)
			}
			css.AddClass("contribution-list")
			ssUI.contributionsContainer.SetSelectionMode(gtk.SELECTION_NONE)
			ssUI.mainContainer, err = gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 6)
			if err != nil {
				return xerrors.Errorf("couldn't build a subsection: %v", err)
			}
			css, err = ssUI.mainContainer.GetStyleContext()
			if err != nil {
				return xerrors.Errorf("couldn't access css style")
			}
			css.AddClass("section-content")
			ssUI.mainContainer.Add(ssUI.contributionsContainer)
			ssUI.mainContainer.Show()
			w.content.Add(ssUI.mainContainer)
			for _, c := range ss.Contributions {
				row, err := w.createContribution(c, MODE_RESULT)
				row.Show()
				if err != nil {
					return xerrors.Errorf("couldn't build a contribution: %v", err)
				}
				ssUI.contributionsContainer.Add(row)
				ssUI.contributionsUI = append(ssUI.contributionsUI, &contributionUI{c, row})
			}
			err = w.addAddRow(&sUI, &ssUI)
			if err != nil {
				return err
			}

			sUI.subsectionsUI = append(sUI.subsectionsUI, &ssUI)
		}
	}

	return nil
}

func (w *mainWindow) addAddRow(s *sectionUI, ss *sectionUI) error {
	// Add + row button
	b, err := gtk.ButtonNewFromIconName("list-add-symbolic", gtk.ICON_SIZE_BUTTON)
	if err != nil {
		return xerrors.Errorf("couldn't create button")
	}
	b.SetHExpand(true)
	b.SetHAlign(gtk.ALIGN_FILL)
	b.Show()
	css, err := b.GetStyleContext()
	if err != nil {
		return xerrors.Errorf("couldn't get css style")
	}
	css.AddClass("flat")
	css.AddClass("add-contribution-button")
	ss.mainContainer.Add(b)

	b.Connect("clicked", func() {
		c := contrib.Contribution{
			// FIXME: Very bad luck we must have to collide with a real ID here...
			ID: rand.Int(),

			ProjectName: s.section.Title,
			ActionName:  ss.section.Title,
		}
		newRow, err := w.createContribution(&c, MODE_EDIT)
		if err != nil {
			fmt.Printf("couldn't create contribution: "+w.errorFormat, err)
		}
		newRow.Show()
		w.model.Append(c)
		nc := ss.contributionsContainer.GetChildren().Length()
		ss.contributionsContainer.Insert(newRow, int(nc))
	})

	return nil
}

type contributionValidationUI struct {
	titleEntry         *gtk.Entry
	titleError         *gtk.Label
	titleErrorRevealer *gtk.Revealer
	linkEntry          *gtk.Entry
	linkError          *gtk.Label
	linkErrorRevealer  *gtk.Revealer
}

func (w *mainWindow) createContribution(c *contrib.Contribution, mode int) (*gtk.ListBoxRow, error) {
	f, err := assets.Data.Open("ui/item.ui")
	if err != nil {
		return nil, xerrors.Errorf("couldn't open: "+w.errorFormat, err)
	}
	defer f.Close()

	builder, err := builder.New(f)
	if err != nil {
		return nil, err
	}

	contributionUI, err := builder.GetBox("item")
	if err != nil {
		return nil, err
	}

	description, err := builder.GetLabel("description")
	if err != nil {
		return nil, err
	}
	description.SetText(c.ReportDescription)
	description.SetVisible(c.ReportDescription != "")

	descriptionEntry, err := builder.GetEntry("descriptionEntry")
	if err != nil {
		return nil, err
	}
	descriptionEntry.SetText(c.ReportDescription)
	descriptionEntry.Connect("changed", func(entry *gtk.Entry, c *contrib.Contribution) {
		c.ReportDescription, _ = descriptionEntry.GetText()
		description.SetText(c.ReportDescription)
		description.SetVisible(c.ReportDescription != "")
	}, c)

	linkErrorRevealer, err := builder.GetRevealer("linkErrorRevealer")
	if err != nil {
		return nil, err
	}
	linkError, err := builder.GetLabel("linkError")
	if err != nil {
		return nil, err
	}

	link, err := builder.GetLinkButton("link")
	if err != nil {
		return nil, err
	}
	link.SetLabel("Link")
	link.SetUri(c.Link)
	link.SetVisible(c.Link != "")

	linkEntry, err := builder.GetEntry("linkEntry")
	if err != nil {
		return nil, err
	}
	linkEntry.SetText(c.Link)

	title, err := builder.GetLabel("title")
	if err != nil {
		return nil, err
	}
	if c.ReportTitle == "" {
		title.SetLabel(c.Title)
	} else {
		title.SetLabel(c.ReportTitle)
	}

	titleErrorRevealer, err := builder.GetRevealer("titleErrorRevealer")
	if err != nil {
		return nil, err
	}
	titleError, err := builder.GetLabel("titleError")
	if err != nil {
		return nil, err
	}

	titleEntry, err := builder.GetEntry("titleEntry")
	if err != nil {
		return nil, err
	}

	if c.ReportTitle == "" {
		titleEntry.SetText(c.Title)
	} else {
		titleEntry.SetText(c.ReportTitle)
	}
	originalTitleExpander, err := builder.GetExpander("originalTitleExpander")
	if err != nil {
		return nil, err
	}
	originalTitleExpander.SetVisible(c.Title != "")

	originalTitle, err := builder.GetLabel("originalTitle")
	if err != nil {
		return nil, err
	}
	originalTitle.SetText(c.Title)

	content, err := builder.GetStack("content")
	if err != nil {
		return nil, err
	}
	if mode == MODE_RESULT {
		content.SetVisibleChildName("result")
	} else {
		content.SetVisibleChildName("edit")
	}

	editButton, err := builder.GetButton("editButton")
	if err != nil {
		return nil, err
	}
	editButton.Connect("clicked", func(b *gtk.Button, w *mainWindow) {
		content.SetVisibleChildName("edit")
	}, w)

	save, err := builder.GetButton("saveButton")
	if err != nil {
		return nil, err
	}

	originalDescriptionExpander, err := builder.GetExpander("originalDescriptionExpander")
	if err != nil {
		return nil, err
	}
	originalDescriptionExpander.SetVisible(c.Description != "")

	originalDescription, err := builder.GetLabel("originalDescription")
	if err != nil {
		return nil, err
	}
	originalDescription.SetMarkup("<span foreground=\"#42464c\" size=\"small\">" + c.Description + "</span>")

	row, err := gtk.ListBoxRowNew()
	if err != nil {
		return nil, err
	}

	deleteButton, err := builder.GetButton("deleteButton")
	if err != nil {
		return nil, err
	}
	deleteButton.Connect("clicked", func(b *gtk.Button, w *mainWindow) {
		w.model.Remove(c)
		err := w.refreshContributions()
		if err != nil {
			log.Printf("ERROR: couldn't refresh contributions: "+w.errorFormat, err)
		}
	}, w)

	moveUp, err := builder.GetButton("moveUp")
	if err != nil {
		return nil, err
	}
	moveUp.SetSensitive(mode == MODE_RESULT)
	moveUp.Connect("clicked", func(b *gtk.Button, w *mainWindow) {
		w.model.MoveUp(c)
		err := w.refreshContributions()
		if err != nil {
			log.Printf("ERROR: couldn't refresh contributions: "+w.errorFormat, err)
		}
	}, w)
	moveDown, err := builder.GetButton("moveDown")
	if err != nil {
		return nil, err
	}
	moveDown.SetSensitive(mode == MODE_RESULT)
	moveDown.Connect("clicked", func(b *gtk.Button, w *mainWindow) {
		w.model.MoveDown(c)
		err := w.refreshContributions()
		if err != nil {
			log.Printf("ERROR: couldn't refresh contributions: "+w.errorFormat, err)
		}
	}, w)

	// TODO: Can this be created after the signal connections below
	// and be included in the closures?
	// That could help with having same widget code together...
	newContrib := contributionValidationUI{
		titleEntry,
		titleError,
		titleErrorRevealer,
		linkEntry,
		linkError,
		linkErrorRevealer,
	}

	titleEntry.Connect("changed", func(entry *gtk.Entry, c *contrib.Contribution) {
		c.ReportTitle, _ = titleEntry.GetText()
		if c.ReportTitle == "" {
			title.SetLabel(c.Title)
		} else {
			title.SetLabel(c.ReportTitle)
		}
		_, err := w.validateContribution(newContrib)
		if err != nil {
			fmt.Printf("couldn't validate contribution:"+w.errorFormat, err)
		}

	}, c)

	save.Connect("clicked", func(b *gtk.Button, w *mainWindow) {
		valid, err := w.validateContribution(newContrib)
		if err != nil {
			fmt.Printf("couldn't validate contribution:"+w.errorFormat, err)
		}
		if valid {
			content.SetVisibleChildName("result")
		}
	}, w)

	linkEntry.Connect("changed", func() {
		c.Link, err = linkEntry.GetText()
		if err != nil {
			fmt.Printf("couldn't set link: "+w.errorFormat, err)
		}
		link.SetUri(c.Link)
		link.SetVisible(c.Link != "")
		_, err := w.validateContribution(newContrib)
		if err != nil {
			fmt.Printf("couldn't validate contribution:"+w.errorFormat, err)
		}
	})

	if err != nil {
		fmt.Printf("couldn't validate contribution:"+w.errorFormat, err)
	}

	row.Add(contributionUI)
	// TODO: list_box_row_set_activatable is not binded in gotk3 yet
	row.SetProperty("activatable", false)

	return row, nil
}

func (w *mainWindow) validateContribution(check contributionValidationUI) (bool, error) {
	valid := true

	css, err := check.titleEntry.GetStyleContext()
	if err != nil {
		return false, xerrors.Errorf("ERROR: couldn't get style context: "+w.errorFormat, err)
	}
	css.RemoveClass("error")
	check.titleErrorRevealer.SetRevealChild(false)
	text, err := check.titleEntry.GetText()
	if err != nil {
		return false, xerrors.Errorf("ERROR: couldn't get entry text: "+w.errorFormat, err)
	}
	valid = valid && text != ""
	if text == "" {
		css.AddClass("error")
		check.titleError.SetText("Title cannot be empty")
		check.titleErrorRevealer.SetRevealChild(true)
	}

	text, err = check.linkEntry.GetText()
	if err != nil {
		return false, xerrors.Errorf("ERROR: couldn't get entry text: "+w.errorFormat, err)
	}
	if text != "" {
		_, err = url.ParseRequestURI(text)
		valid = valid && err == nil
	}
	css, err = check.linkEntry.GetStyleContext()
	if err != nil {
		return false, xerrors.Errorf("ERROR: couldn't get style context: "+w.errorFormat, err)
	}
	css.RemoveClass("error")
	check.linkErrorRevealer.SetRevealChild(false)
	text, err = check.linkEntry.GetText()
	if err != nil {
		return false, xerrors.Errorf("ERROR: couldn't get entry text: "+w.errorFormat, err)
	}
	if text != "" {
		if _, err = url.ParseRequestURI(text); err != nil {
			css.AddClass("error")
			check.linkError.SetText("Link is not valid")
			check.linkErrorRevealer.SetRevealChild(true)
		}
	}

	return valid, nil
}

func (w *mainWindow) editToken() {
	w.mainStack.SetVisibleChildName("tokenPage")
}
