// +build !dev,!external

//go:generate go run --tags generator gen_localization.go ../../

package i18n

import (
	"io/ioutil"
	"log"
	"path/filepath"

	"github.com/leonelquinteros/gotext"
)

func init() {
	var moData []byte
	var loc string
	for _, loc = range setLocales() {
		moFile := filepath.Join(filepath.Join(loc, "LC_MESSAGES", domain+".mo"))
		f, err := Localization.Open(moFile)
		if err != nil {
			continue
		}
		defer f.Close()
		if moData, err = ioutil.ReadAll(f); err != nil {
			log.Printf("%q embedded asset isn't a valid mo file: %v", moFile, err)
			continue
		}
		break
	}

	if moData == nil {
		G = gotext.Get
		GN = gotext.GetN
		return
	}

	if useCgettext == true {
		localeDir, remove, err := tempMoFile(loc, domain, moData)
		if err != nil {
			log.Printf("couldn't create temporary translations for C Gettext: %v", err)
			return
		}
		// We can't remove the temporary directly after CGettext init
		TearDown = remove

		configureCGettext(localeDir, domain)
	}

	mo := gotext.Mo{}
	mo.Parse(moData)
	G = mo.Get
	GN = mo.GetN
}
