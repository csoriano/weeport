// +build ignore

// This programs compiles po to mo files and install them in destination directory. It can be invoked by running go generate
package main

import (
	"flag"
	"fmt"
	"log"
	"os"

	"gitlab.gnome.org/csoriano/weeport/internal/gencode"
	"gitlab.gnome.org/csoriano/weeport/internal/i18n"
)

func main() {
	destdir := flag.String("destdir", "", "Prepend destdir to destination path")
	prefix := flag.String("prefix", gencode.DefaultPrefix, "Prepend prefix (absolute path) between destdir and destination path")
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage of %s:\n    %s DOMAIN PO_SOURCE DESTINATION\n", os.Args[0], os.Args[0])
		fmt.Fprintln(os.Stderr, "Install compiled mo from PO_SOURCE to <DESTDIR>/<PREFIX>/DESTINATION")
	}
	flag.Parse()
	args := flag.Args()
	if len(args) != 3 {
		flag.Usage()
		os.Exit(1)
	}

	moDir, removeMoDir, err := gencode.CreateTemp("mos", true)
	if err != nil {
		log.Fatalf("couldn't create temporary directory for mo files: %v\n", err)
	}
	defer removeMoDir()

	if err = i18n.GenerateMos(args[1], moDir, args[0]); err != nil {
		log.Fatalf("couldn't generate mo files: %+v", err)
	}

	if err := gencode.Install(moDir, args[2], *destdir, *prefix); err != nil {
		log.Fatalln(err)
	}
}
