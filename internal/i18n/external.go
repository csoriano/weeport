// +build external,!dev

package i18n

import (
	"os"
	"path/filepath"
	"strings"

	"github.com/leonelquinteros/gotext"
)

//go:generate go run --tags generator install_localization.go -destdir=$DESTDIR -prefix=$PREFIX weeport ../../po locale/

func init() {
	var dir, loc string
	for _, loc = range setLocales() {
		if dir = findLocaleDir(loc, getLocaleDirs()); dir == "" {
			continue
		}
		break
	}

	configureCGettext(dir, domain)
	gotext.Configure(dir, loc, domain)

	G = gotext.Get
	GN = gotext.GetN
}

// findLocaleDir looks up in localeDirs and stops at first matching locale found for current domain
func findLocaleDir(loc string, localeDirs []string) string {
	for _, dir := range localeDirs {
		if _, err := os.Stat(getMoPath(dir, loc, domain)); err == nil {
			return dir
		}
	}
	return ""
}

func getLocaleDirs() (paths []string) {
	systemDataDirs := strings.Split(os.Getenv("XDG_DATA_DIRS"), ":")
	// we always append /usr/local/share and /usr/share as some platforms don't include them in XDG_DATA_DIRS
	for _, p := range append(systemDataDirs, "/usr/local/share", "/usr/share") {
		// Ignore non absolute paths
		if !filepath.IsAbs(p) {
			continue
		}
		paths = append(paths, filepath.Join(filepath.Dir(p), "locale"),
			filepath.Join(filepath.Dir(p), "locale-langpack"))
	}
	return paths
}
