// +build ignore

// This programs generates po/*.{pot,po} files and localization.go. It can be invoked by running go generate
package main

import (
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"

	"github.com/go-yaml/yaml"
	"github.com/shurcooL/vfsgen"
	"gitlab.gnome.org/csoriano/weeport/internal/gencode"
	"gitlab.gnome.org/csoriano/weeport/internal/i18n"
	"golang.org/x/xerrors"
)

const target = "localization.go"

type i18nConfig struct {
	Name      string
	Files     []string
	Languages []string
}

type poGenerator struct {
	i18nConfig
	root      string
	checkMode bool
}

func (pog poGenerator) Generate(dest string) error {
	// Copy existing po files to temporary directory. This ensures we keep existing translations
	// and thus, can diff
	if pog.checkMode {
		if err := gencode.CopyTree(filepath.Join(pog.root, "po"), dest); err != nil {
			return xerrors.Errorf("couldn't copy existing po/ directory content to destination: %v", err)
		}
	}

	var files []string
	err := filepath.Walk(pog.root, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return xerrors.Errorf("fail to access %q: %v", path, err)
		}
		// Only deal with directories via globbing
		if !info.IsDir() {
			return nil
		}

		for _, pattern := range pog.Files {
			r, err := filepath.Glob(filepath.Join(path, pattern))
			if err != nil {
				return xerrors.Errorf("couldn't list %q in %q: %v", pattern, path, err)
			}
			files = append(files, r...)
		}

		return nil
	})

	if err != nil {
		return err
	}

	// Create temporary pot file
	potfile, removePot, err := gencode.CreateTemp(pog.Name+".pot", false)
	if err != nil {
		return xerrors.Errorf("couldn't create temporary pot file: %v", err)
	}
	defer removePot()
	args := append([]string{
		"--keyword=G", "--keyword=GN", "--add-comments", "--sort-output", "--package-name=" + pog.Name,
		"--output=" + potfile}, files...)
	if out, err := exec.Command("xgettext", args...).CombinedOutput(); err != nil {
		return xerrors.Errorf("couldn't compile pot file: %v. Command output: %s", err, out)
	}

	// Create po
	for _, lang := range pog.Languages {
		pofile := filepath.Join(dest, lang+".po")
		cmd := ""
		var args []string
		if _, err := os.Stat(pofile); err == nil {
			cmd = "msgmerge"
			args = []string{"--update", "--backup=none", pofile, potfile}
		} else {
			cmd = "msginit"
			args = []string{"--input=" + potfile, "--locale=" + lang + ".UTF-8", "--no-translator", "--output=" + pofile}
		}
		if out, err := exec.Command(cmd, args...).CombinedOutput(); err != nil {
			return xerrors.Errorf("couldn't create or refresh %q: %v. Command output: %s", pofile, err, out)
		}
	}

	return nil
}

type moGenerator struct {
	i18nConfig
	root    string
	moCheck bool
}

func (g moGenerator) Generate(dest string) error {
	moDir, removeMoDir, err := gencode.CreateTemp("mos", true)
	if err != nil {
		return xerrors.Errorf("couldn't create temporary directory for mo files: %v", err)
	}
	defer removeMoDir()

	if err = i18n.GenerateMos(filepath.Join(g.root, "po"), moDir, g.Name); err != nil {
		return xerrors.Errorf("couldn't generate mo files: %+v", err)
	}

	// We check manually mo files here, one by one. See comment on generation.
	if g.moCheck {
		originMoDir, removeOriginMoDir, err := gencode.CreateTemp("originMos", true)
		if err != nil {
			return xerrors.Errorf("couldn't create temporary directory for original mo files: %v", err)
		}
		defer removeOriginMoDir()

		if err := filepath.Walk(moDir, func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return xerrors.Errorf("fail to access %q: %v", path, err)
			}
			// Only deal with files
			if info.IsDir() {
				return nil
			}

			// if this is too slow, we can imagine doing this in goroutines
			relativePath := strings.TrimPrefix(path, moDir)
			originEmbeddedF, err := i18n.Localization.Open(relativePath)
			if err != nil {
				return xerrors.Errorf("couldn't find %q in embedded data: %v", relativePath, err)
			}
			parent := filepath.Join(originMoDir, filepath.Dir(relativePath))
			originPath := filepath.Join(originMoDir, relativePath)
			if err := os.MkdirAll(parent, os.ModePerm); err != nil {
				return xerrors.Errorf("couldn't create %q: %v", parent, err)
			}
			originF, err := os.Create(originPath)
			if err != nil {
				return xerrors.Errorf("couldn't create %q: %v", originPath, err)
			}
			if _, err := io.Copy(originF, originEmbeddedF); err != nil {
				originF.Close()
				return xerrors.Errorf("couldn't copy origin content to %q: %v", relativePath, err)
			}
			originF.Close()

			origin, err := exec.Command("msgunfmt", originPath).CombinedOutput()
			if err != nil {
				return xerrors.Errorf("couldn't extract messages from embedded %q: %v. Command output: %s", relativePath, err, originPath)
			}
			if err := ioutil.WriteFile(originPath+".msgunfmt", origin, 0644); err != nil {
				return xerrors.Errorf("couldn't write extracted messages to %q: %v", originPath+".msgunfmt", err)
			}

			generated, err := exec.Command("msgunfmt", path).CombinedOutput()
			if err != nil {
				return xerrors.Errorf("couldn't extract messages from %q: %v. Command output: %s", path, err, generated)
			}
			if err := ioutil.WriteFile(originPath+".generated.msgunfmt", generated, 0644); err != nil {
				return xerrors.Errorf("couldn't write extracted messages to %q: %v", originPath+".generated.msgunfmt", err)
			}

			// Some versions of msgmerge keep the POT-Creation-Date
			identical, err := gencode.AreFilesIdentical(originPath+".msgunfmt", originPath+".generated.msgunfmt", []*regexp.Regexp{regexp.MustCompile("POT-Creation-Date: .*")})
			if err != nil {
				return xerrors.Errorf("couldn't check equality: %v", err)
			}

			if !identical {
				return xerrors.Errorf("original and generated are differents:\n---- %s:\n%s\n---- generated:\n%s", relativePath, origin, generated)
			}

			return nil
		}); err != nil {
			return xerrors.Errorf("check failed: some files differs: %v", err)
		}
	}

	if err := vfsgen.Generate(http.Dir(moDir), vfsgen.Options{
		Filename:        dest,
		PackageName:     "i18n",
		BuildTags:       "!dev,!external",
		VariableName:    "Localization",
		VariableComment: "Localization are the embedded assets withing the binary, corresponding to compiled mo files",
	}); err != nil {
		return xerrors.Errorf("couldn't generate new embedded mo files: %v", err)
	}

	return nil
}

func main() {
	poOnly := flag.Bool("po-only", false, "Only generate PO files")
	checkMode, verbose := gencode.InstallCheckFlags()
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage of %s:\n\n", os.Args[0])
		fmt.Fprintf(os.Stderr, "%s ROOT_PROJECT_DIR\n", os.Args[0])
		fmt.Fprintln(os.Stderr, "Generate $ROOT_PROJECT_DIR/po/ directory content. Definition is made in $ROOT_PROJECT_DIR/po/i18n.yaml.")
	}
	flag.Parse()
	if len(flag.Args()) != 1 {
		flag.Usage()
		os.Exit(1)
	}

	root := flag.Args()[0]
	*checkMode = gencode.IsCheckMode(*checkMode)

	c, err := newConfig(root)
	if err != nil {
		log.Fatalf("ERROR: invalid config: %v", err)
	}

	g := gencode.GenCode{
		Generator: poGenerator{*c, root, *checkMode},
		Dest:      filepath.Join(root, "po"),

		CheckMode: *checkMode,
		Verbose:   *verbose,

		IgnoreInCheckFilters: []*regexp.Regexp{
			// POT-Creation-Date will change in case there is an update to the pot. Skip it then to show real change
			regexp.MustCompile("POT-Creation-Date: .*"),
		},
	}

	if err := g.GenerateAndCheck(); err != nil {
		log.Fatalf("ERROR: couldn't generate po translations: %+v", err)
	}
	defer g.Close()

	if *poOnly {
		return
	}

	// We need to check mo files content manually here and not the aggregated data in localization.go.
	// Indeed, binary format of mo files can change from one version of msmerge to the other, so the strict
	// comparison will fail. We instead msgunfmt and check the content from localization.go and the newly generated
	// mo files.
	g = gencode.GenCode{
		Generator: moGenerator{*c, root, *checkMode},
		Dest:      target,

		CheckMode: false,
		Verbose:   *verbose,
	}

	if err := g.GenerateAndCheck(); err != nil {
		log.Fatalf("ERROR: couldn't generate mo content: %+v", err)
	}
	defer g.Close()
}

func newConfig(root string) (*i18nConfig, error) {
	b, err := ioutil.ReadFile(filepath.Join(root, "po", "i18n.yaml"))
	if err != nil {
		return nil, xerrors.Errorf("couldn't open i18n source definition file: %v", err)
	}

	c := i18nConfig{}
	err = yaml.Unmarshal(b, &c)
	if err != nil {
		return nil, xerrors.Errorf("wrongly formatted i18n definition file: %v", err)
	}

	if c.Name == "" {
		c.Name = "messages"
	}
	if c.Files == nil {
		return nil, xerrors.New("no files or glob pattern found. i18n.yaml file need a 'files' entry.")
	}
	if c.Languages == nil {
		return nil, xerrors.Errorf("no languages found. i18n.yaml file need a 'languages' entry.")
	}

	return &c, nil
}
