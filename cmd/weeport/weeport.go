package main

import (
	"flag"
	"fmt"
	"os"
	"strings"

	"gitlab.gnome.org/csoriano/weeport/internal/i18n"

	"gitlab.gnome.org/csoriano/weeport/internal/app"
)

func main() {
	defer i18n.TearDown()

	// There is no way of adding command line options with gogtk3 bindings, but we still want to add additional flags.
	// Use thus a flagset and handle the help message ourself, passing it to GTK after cleaning up unknown flags to GTK.
	noGTKFlags := flag.FlagSet{Usage: flag.Usage}
	debug := noGTKFlags.Bool("d", false, "debug mode")
	// We will never return an error as ContinueOnError is the default for flagset
	noGTKFlags.Parse(os.Args[1:])

	var options []app.WeePortOption
	if *debug {
		options = append(options, app.WithDebug)
	}

	// cleanup unknown options from GtkObject.Run() and detect "help" manually to print additional info,
	// but let GTK handling --help as well for GTK specific flags
	// There is no way to use defer() to print those options after the GTK usage, as gapplication
	// is calling os.Exit() directly.
	var args []string
	for _, arg := range os.Args {
		if arg == "-d" {
			continue
		}
		if strings.Replace(arg, "-", "", -1) == "help" {
			// Help usage is normally set to stderr, but GTK does it on stdout, so force it here.
			noGTKFlags.SetOutput(os.Stdout)
			noGTKFlags.Usage()
			fmt.Println()
			noGTKFlags.PrintDefaults()
			fmt.Println("\n\nGTK specific options:")
		}
		args = append(args, arg)
	}

	app := app.New(options...)
	app.GtkObject.Run(args)
}
