module gitlab.gnome.org/csoriano/weeport

require (
	github.com/go-yaml/yaml v2.1.0+incompatible
	github.com/gotk3/gotk3 v0.0.0-20190108052711-d09d58ef3476
	github.com/kr/pretty v0.1.0 // indirect
	github.com/leonelquinteros/gotext v1.4.0
	github.com/shurcooL/httpfs v0.0.0-20181222201310-74dc9339e414 // indirect
	github.com/shurcooL/vfsgen v0.0.0-20181202132449-6a9ea43bcacd
	github.com/xanzy/go-gitlab v0.14.0
	golang.org/x/tools v0.0.0-20190208222737-3744606dbb67 // indirect
	golang.org/x/xerrors v0.0.0-20190208160001-334af843aad9
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
	gopkg.in/yaml.v2 v2.2.2 // indirect
)
